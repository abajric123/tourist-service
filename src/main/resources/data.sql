DROP TABLE IF EXISTS countries;
DROP TABLE IF EXISTS cities;

CREATE TABLE countries (
  id INT PRIMARY KEY,
  country_name VARCHAR(250) NOT NULL
);

CREATE TABLE cities (
  id INT PRIMARY KEY,
  city_name VARCHAR(250) NOT NULL,
  country_id INT,
   FOREIGN KEY (country_id) REFERENCES countries(id)
);

INSERT INTO countries (id,country_name) VALUES
  (1,'Bosna'),(2,'Francuska'),(3,'Italija');

  INSERT INTO cities (city_name,id,country_id) VALUES
      ('Sarajevo',7,1),('Pariz',8,2),('Rim',9,3);
